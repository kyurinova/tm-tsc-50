package ru.tsc.kyurinova.tm.exception.empty;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty.");
    }

}
