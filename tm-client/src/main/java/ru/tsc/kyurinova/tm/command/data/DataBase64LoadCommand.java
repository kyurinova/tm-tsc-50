package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;

public class DataBase64LoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-load-base64";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataBase64Load(session);
    }

    @Override
    public @Nullable
    Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
