package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskEndpoint().existsByIdTask(session, id)) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().updateByIdTask(session, id, name, description);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
