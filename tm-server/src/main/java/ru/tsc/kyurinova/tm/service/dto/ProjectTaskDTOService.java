package ru.tsc.kyurinova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public ProjectTaskDTOService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @Override
    public void bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        try {
            entityManager.getTransaction().begin();
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        try {
            entityManager.getTransaction().begin();
            taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        removeAllTaskByProjectId(userId, projectId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllTaskByProjectId(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        try {
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

}
