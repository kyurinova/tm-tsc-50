package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.dto.IUserDTOService;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.service.dto.UserDTOService;
import ru.tsc.kyurinova.tm.util.HashUtil;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserDTOService userService;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "test@mail.com";

    public UserServiceTest() {
        propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final LogService logService = new LogService(connectionService);
        userService = new UserDTOService(connectionService, logService, propertyService);
        user = userService.create(userLogin, "userTest", userEmail);
        userId = user.getId();
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user.getId(), userService.findById(userId).getId());
        Assert.assertEquals(user.getId(), userService.findByIndex(0).getId());
        Assert.assertEquals(user.getId(), userService.findByLogin(userLogin).getId());
        Assert.assertEquals(user.getId(), userService.findByEmail(userEmail).getId());
    }

    @Test
    public void removeUserByIdTest() {
        Assert.assertNotNull(userId);
        userService.removeById(userId);
        Assert.assertFalse(userService.existsById(userId));
    }

    @Test
    public void existsUserByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertTrue(userService.existsById(userId));
    }

    @Test
    public void createNoEmailTest() {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserTest";
        @NotNull final String newUserPassword = "newPassword";
        @NotNull final UserDTO newUser = userService.create(newUserLogin, newUserPassword);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
        Assert.assertEquals(2, userService.findAll().size());
        userService.remove(newUser);
    }

    @Test
    public void createTest() {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserTest";
        @NotNull final String newUserEmail = "newTest@mail.com";
        @NotNull final String newUserPassword = "newPassword";
        @NotNull final UserDTO newUser = userService.create(newUserLogin, newUserPassword, newUserEmail);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        Assert.assertEquals(newUserEmail, newUser.getEmail());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
        Assert.assertEquals(2, userService.findAll().size());
        userService.remove(newUser);
    }

    @Test
    public void setPasswordTest() {
        @NotNull final String newPassword = "userTest";
        userService.setPassword(userId, newPassword);
        Assert.assertNotEquals(HashUtil.salt("test", 3, "test"), user.getPasswordHash());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newPassword), user.getPasswordHash());
    }

    @Test
    public void updateUserTest() {
        @NotNull final String newFirstName = "newFirstNameTest";
        @NotNull final String newLastName = "newLastNameTest";
        @NotNull final String newMiddleName = "newMiddleNameTest";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        userService.updateUser(userId, newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(newFirstName, userService.findById(userId).getFirstName());
        Assert.assertEquals(newLastName, userService.findById(userId).getLastName());
        Assert.assertEquals(newMiddleName, userService.findById(userId).getMiddleName());
    }

    @Test
    public void lockUnlockByLoginTest() {
        Assert.assertEquals("N", user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertEquals("Y", userService.findByLogin(userLogin).getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertEquals("N", userService.findByLogin(userLogin).getLocked());
    }

    @After
    public void after() {
        userService.removeById(userId);
    }

}
