package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.tsc.kyurinova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.UserDTORepository;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class ProjectRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private ProjectDTO project;

    @NotNull
    private String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "Project for test";

    @NotNull
    private final String userId;

    public EntityManager GetEntityManager() {
        return connectionService.getEntityManager();
    }

    public ProjectRepositoryTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        @NotNull final UserDTO user = new UserDTO();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt("test", 3, "test"));
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void before() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setCreated(new Timestamp(project.getCreated().getTime()));
        projectRepository.add(userId, project);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findProjectTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project.getId(), projectRepository.findById(userId, projectId).getId());
        Assert.assertEquals(project.getId(), projectRepository.findByName(userId, projectName).getId());
    }

    @Test
    public void existsProjectTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectRepository.findById(userId, projectId));
        Assert.assertNotNull(projectRepository.findByIndex(userId, 0));
    }

    @Test
    public void removeProjectByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.removeById(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(projectRepository.findAllUserId(userId).isEmpty());
    }

    @Test
    public void removeProjectByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.removeByName(userId, projectName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void startByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.startById(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        projectRepository.startByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.startByName(userId, projectName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.finishById(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        projectRepository.finishByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.finishByName(userId, projectName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectRepository.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.COMPLETED);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findById(userId, projectId).getStatus());
        projectRepository.changeStatusById(userId, projectId, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        projectRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
        projectRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectRepository.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        Assert.assertEquals(Status.IN_PROGRESS, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.COMPLETED);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByName(userId, projectName).getStatus());
        projectRepository.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, projectRepository.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        IProjectDTORepository projectDTORepositoryNew = new ProjectDTORepository(entityManager);
        IUserDTORepository userDTORepositoryNew = new UserDTORepository(entityManager);
        entityManager.getTransaction().begin();
        projectDTORepositoryNew.clearUserId(userId);
        if (entityManager.getTransaction().isActive())
            entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        userDTORepositoryNew.clear();
        if (entityManager.getTransaction().isActive())
            entityManager.getTransaction().commit();
        entityManager.close();
    }


}
