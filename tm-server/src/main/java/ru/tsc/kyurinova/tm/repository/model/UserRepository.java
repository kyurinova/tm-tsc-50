package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.model.IUserRepository;
import ru.tsc.kyurinova.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository implements IUserRepository {

    protected EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public void update(@NotNull User user) {
        entityManager.merge(user);
    }

    @Override
    public User findByLogin(@NotNull String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable
    User findByEmail(@NotNull String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager.remove(findByLogin(login));
    }

    @Override
    public void remove(@NotNull User user) {
        entityManager.remove(user);
    }

    @Override
    public @Nullable
    List<User> findAll() {
        return entityManager
                .createQuery("SELECT m FROM User m", User.class)
                .getResultList();

    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM User")
                .executeUpdate();
    }

    @Override
    public @Nullable
    User findById(@NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.id= :id";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable
    User findByIndex(@NotNull Integer index) {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByIndex(@NotNull Integer index) {
        entityManager.remove(findByIndex(index));
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM User m";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult().intValue();
    }

}
