package ru.tsc.kyurinova.tm.service;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.listener.EntityListener;
import ru.tsc.kyurinova.tm.listener.JmsLoggerProducer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public final class LogService implements ILogService {

    @NotNull
    private static final String FILE_NAME = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";
    @NotNull
    private static final String COMMANDS_FILE = "./commands.txt";

    @NotNull
    private static final String MESSAGES = "MESSAGES";
    @NotNull
    private static final String MESSAGES_FILE = "./messages.txt";

    @NotNull
    private static final String ERRORS = "ERRORS";
    @NotNull
    private static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();
    @NotNull
    private final LogManager manager = LogManager.getLogManager();
    @NotNull
    private final Logger root = Logger.getLogger("");
    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);
    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    @NotNull
    private final IConnectionService connectionService;

    {
        init();
        registry(errors, ERRORS_FILE, true);
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
    }

    public LogService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            @NotNull final InputStream inputStream = LogService.class.getResourceAsStream(FILE_NAME);
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void initJmsLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer();
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);
        @NotNull final EntityManagerFactory entityManagerFactory = connectionService.getEntityManager().getEntityManagerFactory();
        @NotNull final SessionFactoryImpl sessionFactoryImpl = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactoryImpl.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

}
