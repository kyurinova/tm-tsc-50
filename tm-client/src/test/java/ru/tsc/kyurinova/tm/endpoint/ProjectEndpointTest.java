package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kyurinova.tm.marker.SoapCategory;

public class ProjectEndpointTest {

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String projectName = "projectTest";

    @NotNull
    private final String projectDescription = "projectTestDescription";

    @Nullable
    private String projectId;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private SessionDTO session;

    @NotNull
    private UserDTO user;


    public ProjectEndpointTest() {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
    }

    @Before
    public void before() {
        user = adminUserEndpoint.createAdminUser("test", "test");
        session = sessionEndpoint.openSession("test", "test");
        projectEndpoint.createProjectDescr(session, projectName, projectDescription);
        projectId = projectEndpoint.findByNameProject(session, projectName).getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllProjectTest() {
        Assert.assertEquals(1, projectEndpoint.findAllProject(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProjectTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findByIdProject(session, projectId));
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findByNameProject(session, projectName));
        Assert.assertNotNull(projectEndpoint.findByIndexProject(session, 0));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByIdTest() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findByIdProject(session, projectId));
        projectEndpoint.removeByIdProject(session, projectId);
        Assert.assertTrue(projectEndpoint.findAllProject(session).isEmpty());
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(projectEndpoint.findByIndexProject(session, 0));
        projectEndpoint.removeByIndexProject(session, 0);
        Assert.assertTrue(projectEndpoint.findAllProject(session).isEmpty());
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void removeProjectByNameTest() {
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findByNameProject(session, projectName));
        projectEndpoint.removeByNameProject(session, projectName);
        Assert.assertTrue(projectEndpoint.findAllProject(session).isEmpty());
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void createTest() {
        projectEndpoint.createProjectDescr(session, "newTest", "newTest");
        @NotNull final String currProjectId = projectEndpoint.findByNameProject(session, "newTest").getId();
        Assert.assertNotNull(currProjectId);
        Assert.assertNotNull(projectEndpoint.findByIdProject(session, currProjectId));
        projectEndpoint.removeByIdProject(session, currProjectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdProjectTest() {
        Assert.assertNotNull(projectId);
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectEndpoint.updateByIdProject(session, projectId, newProjectName, newProjectDescription);
        @NotNull final ProjectDTO updatedProject = projectEndpoint.findByIdProject(session, projectId);
        Assert.assertEquals(projectId, updatedProject.getId());
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIndexProjectTest() {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectEndpoint.updateByIndexProject(session, 0, newProjectName, newProjectDescription);
        @NotNull final ProjectDTO updatedProject = projectEndpoint.findByIndexProject(session, 0);
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByIdTest() {
        Assert.assertNotNull(projectId);
        projectEndpoint.startByIdProject(session, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findByIdProject(session, projectId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByIndexTest() {
        projectEndpoint.startByIndexProject(session, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findByIndexProject(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startProjectByNameTest() {
        Assert.assertNotNull(projectName);
        projectEndpoint.startByNameProject(session, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findByNameProject(session, projectName).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByIdTest() {
        Assert.assertNotNull(projectId);
        projectEndpoint.finishByIdProject(session, projectId);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findByIdProject(session, projectId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByIndexTest() {
        projectEndpoint.finishByIndexProject(session, 0);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findByIndexProject(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishProjectByNameTest() {
        Assert.assertNotNull(projectName);
        projectEndpoint.finishByNameProject(session, projectName);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findByNameProject(session, projectName).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(projectId);
        projectEndpoint.changeStatusByIdProject(session, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.findByIdProject(session, projectId).getStatus());
        projectEndpoint.changeStatusByIdProject(session, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findByIdProject(session, projectId).getStatus());
        projectEndpoint.changeStatusByIdProject(session, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findByIdProject(session, projectId).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByIndexTest() {
        Assert.assertNotNull(session);
        projectEndpoint.changeStatusByIndexProject(session, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.findByIndexProject(session, 0).getStatus());
        projectEndpoint.changeStatusByIndexProject(session, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findByIndexProject(session, 0).getStatus());
        projectEndpoint.changeStatusByIndexProject(session, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findByIndexProject(session, 0).getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeProjectStatusByNameTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(projectName);
        projectEndpoint.changeStatusByNameProject(session, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.findByNameProject(session, projectName).getStatus());
        projectEndpoint.changeStatusByNameProject(session, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.findByNameProject(session, projectName).getStatus());
        projectEndpoint.changeStatusByNameProject(session, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.findByNameProject(session, projectName).getStatus());
    }

    @After
    public void after() {
        if (projectId != null)
            projectEndpoint.removeByIdProject(session, projectId);
//        adminUserEndpoint.removeByLoginUser(session,"test");
        sessionEndpoint.closeSession(session);

    }

}
