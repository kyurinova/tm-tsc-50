package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kyurinova.tm.marker.SoapCategory;

public class UserEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final UserEndpoint userEndpoint;

    @NotNull
    private SessionDTO session;

    @NotNull
    private String userId;

    @NotNull
    private final String userLogin = "test";

    @NotNull
    private final String userPassword = "test";

    public UserEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession(userLogin, userPassword);
        userId = session.getUserId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdUserTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(userEndpoint.findByIdUser(session, userId));
    }

    @Test
    @Category(SoapCategory.class)
    public void findByLoginUserTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(userEndpoint.findByLoginUser(session, userLogin));
    }

    @Test
    @Category(SoapCategory.class)
    public void existsByIdUserTest() {
        Assert.assertNotNull(session);
        Assert.assertTrue(userEndpoint.existsByIdUser(session, userId));
    }

    @Test
    @Category(SoapCategory.class)
    public void isLoginExistsTest() {
        Assert.assertNotNull(session);
        userEndpoint.isLoginExistsUser(session, userLogin);
    }

    @After
    public void after() {
        sessionEndpoint.closeSession(session);
    }

}