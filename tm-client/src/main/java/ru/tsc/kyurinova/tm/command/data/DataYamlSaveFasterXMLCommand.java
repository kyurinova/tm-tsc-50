package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

public class DataYamlSaveFasterXMLCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-yaml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to yaml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML SAVE]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataYamlSaveFasterXML(session);
    }

}
