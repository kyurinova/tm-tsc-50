package ru.tsc.kyurinova.tm.exception.system;

import ru.tsc.kyurinova.tm.constant.TerminalConst;
import ru.tsc.kyurinova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use '" + TerminalConst.HELP + "' to display list of terminal commands.");
    }

}
